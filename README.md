Eclipse Scout - Oomph
=====================

[Eclipse Scout] [1] is a mature and open framework for modern, service oriented business applications.
It substantially boosts developer productivity and is simple to learn.

This Repository **Eclipse Scout Oomph** contains the source for the setup script using Eclipse Oomph (a.k.a Eclipse Installer).


Downloads
---------

There is no direct download for this repository. You can access the content with any git client.

The easiest way to start with Eclipse Scout is to download _Eclipse for Scout Developers Eclipse for Scout Developers_ on the [Eclipse downloads page] [2].


Content
-------

The main file of this repository is the [Scout.setup] [3] file.


Documentation & Links
---------------------

* [Eclipse Scout Wiki] [4]
* [Eclipse Scout Forum] [5]
* [Eclipse Bugzilla] [6] (Product=Scout; Component=Scout Docs)


Contribution Guidelines
-----------------------

We welcome any kind of contributions (Bug report, documentation, code contribution...).
Please read the [Eclipse Scout Contribution page] [7] to know more about it.

The contribution process of Eclipse Scout is hosted on tools deployed by the Eclipse Foundation (involing [Bugzilla] [6], Gerrit, Hudson, MediaWiki...).

External tools like the GitHub tracker and pull requests are not supported.


Get in Touch
------------

To get in touch with the Eclipse Scout community, please open a thread in the [Eclipse Scout Forum] [5] or send a mail to [our mailing list] [8]: scout-dev@eclipse.org


License
-------

[Eclipse Public License (EPL) v1.0] [9]


[1]: http://eclipse.org/scout/
[2]: http://www.eclipse.org/downloads/
[3]: Scout.setup
[4]: http://wiki.eclipse.org/Scout/
[5]: http://eclipse.org/forums/eclipse.scout
[6]: http://bugs.eclipse.org/
[7]: http://wiki.eclipse.org/Scout/Contribution
[8]: http://dev.eclipse.org/mailman/listinfo/scout-dev
[9]: http://wiki.eclipse.org/EPL
